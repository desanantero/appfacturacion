USE [master]
GO

/****** 
POR FAVOR CAMBIE LAS RUTAS DE LOS LOGS SI ES NECESARIO
******/
CREATE DATABASE [AppFacturacion]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'AppFacturacion', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.SQLEXPRESS\MSSQL\DATA\AppFacturacion.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'AppFacturacion_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.SQLEXPRESS\MSSQL\DATA\AppFacturacion_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO

IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [AppFacturacion].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO

ALTER DATABASE [AppFacturacion] SET ANSI_NULL_DEFAULT OFF 
GO

ALTER DATABASE [AppFacturacion] SET ANSI_NULLS OFF 
GO

ALTER DATABASE [AppFacturacion] SET ANSI_PADDING OFF 
GO

ALTER DATABASE [AppFacturacion] SET ANSI_WARNINGS OFF 
GO

ALTER DATABASE [AppFacturacion] SET ARITHABORT OFF 
GO

ALTER DATABASE [AppFacturacion] SET AUTO_CLOSE OFF 
GO

ALTER DATABASE [AppFacturacion] SET AUTO_SHRINK OFF 
GO

ALTER DATABASE [AppFacturacion] SET AUTO_UPDATE_STATISTICS ON 
GO

ALTER DATABASE [AppFacturacion] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO

ALTER DATABASE [AppFacturacion] SET CURSOR_DEFAULT  GLOBAL 
GO

ALTER DATABASE [AppFacturacion] SET CONCAT_NULL_YIELDS_NULL OFF 
GO

ALTER DATABASE [AppFacturacion] SET NUMERIC_ROUNDABORT OFF 
GO

ALTER DATABASE [AppFacturacion] SET QUOTED_IDENTIFIER OFF 
GO

ALTER DATABASE [AppFacturacion] SET RECURSIVE_TRIGGERS OFF 
GO

ALTER DATABASE [AppFacturacion] SET  DISABLE_BROKER 
GO

ALTER DATABASE [AppFacturacion] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO

ALTER DATABASE [AppFacturacion] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO

ALTER DATABASE [AppFacturacion] SET TRUSTWORTHY OFF 
GO

ALTER DATABASE [AppFacturacion] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO

ALTER DATABASE [AppFacturacion] SET PARAMETERIZATION SIMPLE 
GO

ALTER DATABASE [AppFacturacion] SET READ_COMMITTED_SNAPSHOT OFF 
GO

ALTER DATABASE [AppFacturacion] SET HONOR_BROKER_PRIORITY OFF 
GO

ALTER DATABASE [AppFacturacion] SET RECOVERY SIMPLE 
GO

ALTER DATABASE [AppFacturacion] SET  MULTI_USER 
GO

ALTER DATABASE [AppFacturacion] SET PAGE_VERIFY CHECKSUM  
GO

ALTER DATABASE [AppFacturacion] SET DB_CHAINING OFF 
GO

ALTER DATABASE [AppFacturacion] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO

ALTER DATABASE [AppFacturacion] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO

ALTER DATABASE [AppFacturacion] SET DELAYED_DURABILITY = DISABLED 
GO

ALTER DATABASE [AppFacturacion] SET QUERY_STORE = OFF
GO

ALTER DATABASE [AppFacturacion] SET  READ_WRITE 
GO





USE [AppFacturacion]
GO
/****** CREAMOS TABLAS E INSERTAMOS REGISTROS ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_client](
	[cli_identification] [varchar](15) NOT NULL,
	[cli_name] [varchar](50) NULL,
	[cli_birthdate] [datetime2](7) NULL,
 CONSTRAINT [PK_tbl_client] PRIMARY KEY CLUSTERED 
(
	[cli_identification] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_product]    Script Date: 2/06/2021 11:20:06 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_product](
	[pr_id] [int] IDENTITY(1,1) NOT NULL,
	[pr_name] [varchar](50) NOT NULL,
	[pr_price] [real] NOT NULL,
 CONSTRAINT [PK_tbl_product] PRIMARY KEY CLUSTERED 
(
	[pr_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_purchase]    Script Date: 2/06/2021 11:20:06 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_purchase](
	[pur_id] [int] IDENTITY(1,1) NOT NULL,
	[pur_cli_id] [varchar](15) NOT NULL,
	[pur_date] [datetime2](7) NOT NULL,
	[pur_total_price] [real] NOT NULL,
	[pur_paid_price] [real] NOT NULL,
 CONSTRAINT [PK_tbl_purchase] PRIMARY KEY CLUSTERED 
(
	[pur_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_purchase_stock_details]    Script Date: 2/06/2021 11:20:06 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_purchase_stock_details](
	[psd_stock_id] [int] NOT NULL,
	[psd_purchase_id] [int] NOT NULL,
	[psd_id] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_purchase_stock_details] PRIMARY KEY CLUSTERED 
(
	[psd_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_stock]    Script Date: 2/06/2021 11:20:06 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_stock](
	[st_product_id] [int] NOT NULL,
	[st_is_sold] [bit] NOT NULL,
	[st_id] [int] IDENTITY(1,1) NOT NULL,
	[st_date_expire] [datetime2](7) NULL,
	[st_bar_code] [varchar](50) NOT NULL,
 CONSTRAINT [PK_tbl_stock] PRIMARY KEY CLUSTERED 
(
	[st_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[tbl_client] ([cli_identification], [cli_name], [cli_birthdate]) VALUES (N'1073123456', N'Adriana Milena Ocampo', CAST(N'1968-07-31T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[tbl_client] ([cli_identification], [cli_name], [cli_birthdate]) VALUES (N'1073825150', N'José Carlos Peñata', CAST(N'1994-08-13T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[tbl_client] ([cli_identification], [cli_name], [cli_birthdate]) VALUES (N'1073987654', N'Andres Manuel Romero', CAST(N'1984-08-10T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[tbl_client] ([cli_identification], [cli_name], [cli_birthdate]) VALUES (N'91909876', N'Maria Gabriela Morales', CAST(N'1999-01-09T00:00:00.0000000' AS DateTime2))
GO
SET IDENTITY_INSERT [dbo].[tbl_product] ON 

INSERT [dbo].[tbl_product] ([pr_id], [pr_name], [pr_price]) VALUES (1, N'Licencia de Windows 10 Pro x2 años', 455000)
INSERT [dbo].[tbl_product] ([pr_id], [pr_name], [pr_price]) VALUES (2, N'Licencia de Windos 10 Home', 399999)
INSERT [dbo].[tbl_product] ([pr_id], [pr_name], [pr_price]) VALUES (3, N'Licencia para Visual Studio Profesional x2 años', 4999999)
INSERT [dbo].[tbl_product] ([pr_id], [pr_name], [pr_price]) VALUES (4, N'Licencia Azure Clud Service', 1500000)
SET IDENTITY_INSERT [dbo].[tbl_product] OFF
GO
SET IDENTITY_INSERT [dbo].[tbl_purchase] ON 

INSERT [dbo].[tbl_purchase] ([pur_id], [pur_cli_id], [pur_date], [pur_total_price], [pur_paid_price]) VALUES (2, N'1073825150', CAST(N'2021-06-02T00:00:00.0000000' AS DateTime2), 455000, 500000)
INSERT [dbo].[tbl_purchase] ([pur_id], [pur_cli_id], [pur_date], [pur_total_price], [pur_paid_price]) VALUES (4, N'1073987654', CAST(N'2000-03-08T00:00:00.0000000' AS DateTime2), 4999999, 5000000)
INSERT [dbo].[tbl_purchase] ([pur_id], [pur_cli_id], [pur_date], [pur_total_price], [pur_paid_price]) VALUES (5, N'1073825150', CAST(N'2000-02-10T00:00:00.0000000' AS DateTime2), 4999999, 5000000)
INSERT [dbo].[tbl_purchase] ([pur_id], [pur_cli_id], [pur_date], [pur_total_price], [pur_paid_price]) VALUES (6, N'1073825150', CAST(N'2000-02-10T00:00:00.0000000' AS DateTime2), 3999999, 4000000)
INSERT [dbo].[tbl_purchase] ([pur_id], [pur_cli_id], [pur_date], [pur_total_price], [pur_paid_price]) VALUES (7, N'1073825150', CAST(N'2021-06-03T00:00:00.0000000' AS DateTime2), 3999999, 4000000)
INSERT [dbo].[tbl_purchase] ([pur_id], [pur_cli_id], [pur_date], [pur_total_price], [pur_paid_price]) VALUES (8, N'1073825150', CAST(N'2021-06-20T00:00:00.0000000' AS DateTime2), 5000000, 5000000)
SET IDENTITY_INSERT [dbo].[tbl_purchase] OFF
GO
SET IDENTITY_INSERT [dbo].[tbl_purchase_stock_details] ON 

INSERT [dbo].[tbl_purchase_stock_details] ([psd_stock_id], [psd_purchase_id], [psd_id]) VALUES (10, 2, 1)
INSERT [dbo].[tbl_purchase_stock_details] ([psd_stock_id], [psd_purchase_id], [psd_id]) VALUES (15, 2, 2)
INSERT [dbo].[tbl_purchase_stock_details] ([psd_stock_id], [psd_purchase_id], [psd_id]) VALUES (8, 4, 3)
INSERT [dbo].[tbl_purchase_stock_details] ([psd_stock_id], [psd_purchase_id], [psd_id]) VALUES (7, 6, 4)
INSERT [dbo].[tbl_purchase_stock_details] ([psd_stock_id], [psd_purchase_id], [psd_id]) VALUES (4, 6, 5)
SET IDENTITY_INSERT [dbo].[tbl_purchase_stock_details] OFF
GO
SET IDENTITY_INSERT [dbo].[tbl_stock] ON 

INSERT [dbo].[tbl_stock] ([st_product_id], [st_is_sold], [st_id], [st_date_expire], [st_bar_code]) VALUES (1, 0, 1, CAST(N'2022-06-01T00:00:00.0000000' AS DateTime2), N'1234')
INSERT [dbo].[tbl_stock] ([st_product_id], [st_is_sold], [st_id], [st_date_expire], [st_bar_code]) VALUES (1, 0, 4, CAST(N'2022-06-30T00:00:00.0000000' AS DateTime2), N'4321')
INSERT [dbo].[tbl_stock] ([st_product_id], [st_is_sold], [st_id], [st_date_expire], [st_bar_code]) VALUES (1, 0, 5, NULL, N'4312')
INSERT [dbo].[tbl_stock] ([st_product_id], [st_is_sold], [st_id], [st_date_expire], [st_bar_code]) VALUES (1, 0, 6, CAST(N'2023-12-12T00:00:00.0000000' AS DateTime2), N'1243')
INSERT [dbo].[tbl_stock] ([st_product_id], [st_is_sold], [st_id], [st_date_expire], [st_bar_code]) VALUES (2, 0, 7, CAST(N'2022-02-22T00:00:00.0000000' AS DateTime2), N'7890')
INSERT [dbo].[tbl_stock] ([st_product_id], [st_is_sold], [st_id], [st_date_expire], [st_bar_code]) VALUES (2, 0, 8, CAST(N'2023-01-03T00:00:00.0000000' AS DateTime2), N'7809')
INSERT [dbo].[tbl_stock] ([st_product_id], [st_is_sold], [st_id], [st_date_expire], [st_bar_code]) VALUES (2, 1, 10, CAST(N'2023-09-09T00:00:00.0000000' AS DateTime2), N'0987')
INSERT [dbo].[tbl_stock] ([st_product_id], [st_is_sold], [st_id], [st_date_expire], [st_bar_code]) VALUES (2, 0, 11, CAST(N'2022-02-22T00:00:00.0000000' AS DateTime2), N'0978')
INSERT [dbo].[tbl_stock] ([st_product_id], [st_is_sold], [st_id], [st_date_expire], [st_bar_code]) VALUES (1, 0, 12, CAST(N'2021-10-10T00:00:00.0000000' AS DateTime2), N'3421')
INSERT [dbo].[tbl_stock] ([st_product_id], [st_is_sold], [st_id], [st_date_expire], [st_bar_code]) VALUES (1, 0, 13, CAST(N'2021-12-02T00:00:00.0000000' AS DateTime2), N'3344')
INSERT [dbo].[tbl_stock] ([st_product_id], [st_is_sold], [st_id], [st_date_expire], [st_bar_code]) VALUES (3, 1, 15, NULL, N'6565')
INSERT [dbo].[tbl_stock] ([st_product_id], [st_is_sold], [st_id], [st_date_expire], [st_bar_code]) VALUES (4, 1, 16, NULL, N'7777')
SET IDENTITY_INSERT [dbo].[tbl_stock] OFF
GO
ALTER TABLE [dbo].[tbl_purchase]  WITH CHECK ADD  CONSTRAINT [FK_tbl_purchase_tbl_purchase] FOREIGN KEY([pur_cli_id])
REFERENCES [dbo].[tbl_client] ([cli_identification])
GO
ALTER TABLE [dbo].[tbl_purchase] CHECK CONSTRAINT [FK_tbl_purchase_tbl_purchase]
GO
ALTER TABLE [dbo].[tbl_purchase_stock_details]  WITH CHECK ADD  CONSTRAINT [FK_tbl_purchase_stock_details_tbl_purchase] FOREIGN KEY([psd_purchase_id])
REFERENCES [dbo].[tbl_purchase] ([pur_id])
GO
ALTER TABLE [dbo].[tbl_purchase_stock_details] CHECK CONSTRAINT [FK_tbl_purchase_stock_details_tbl_purchase]
GO
ALTER TABLE [dbo].[tbl_purchase_stock_details]  WITH CHECK ADD  CONSTRAINT [FK_tbl_purchase_stock_details_tbl_stock] FOREIGN KEY([psd_stock_id])
REFERENCES [dbo].[tbl_stock] ([st_id])
GO
ALTER TABLE [dbo].[tbl_purchase_stock_details] CHECK CONSTRAINT [FK_tbl_purchase_stock_details_tbl_stock]
GO
ALTER TABLE [dbo].[tbl_stock]  WITH CHECK ADD  CONSTRAINT [FK_tbl_stock_tbl_stock] FOREIGN KEY([st_product_id])
REFERENCES [dbo].[tbl_product] ([pr_id])
GO
ALTER TABLE [dbo].[tbl_stock] CHECK CONSTRAINT [FK_tbl_stock_tbl_stock]
GO
