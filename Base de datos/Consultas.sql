USE [AppFacturacion]
GO

-- Obtener la lista de precios de todos los productos
Select [dbo].[tbl_product].pr_price as 'Precio' from [dbo].[tbl_product];


--Obtener la lista de productos cuya existencia en el inventario haya llegado al m�nimo permitido (5 unidades)
(Select distinct p.pr_name as 'Productos en stock minimo' from [dbo].[tbl_product] as p
join [dbo].[tbl_stock] as s on p.pr_id = s.st_product_id
where 5>= (SELECT count(*) FROM [tbl_stock] where st_is_sold=0 and st_product_id=p.pr_id));


--Obtener una lista de clientes no mayores de 35 a�os que hayan realizado compras entre
--el 1 de febrero de 2000 y el 25 de mayo de 2000
Select distinct c.cli_name as 'Clientes' from [dbo].[tbl_client] as c
join [dbo].[tbl_purchase] as p
on p.pur_cli_id = c.cli_identification
where p.pur_date >='2000-02-01' and p.pur_date<='2000-05-25'  and (SELECT FLOOR(DATEDIFF(DAY, c.cli_birthdate, GETDATE()) / 365.25))<=35


 --Obtener el valor total vendido por cada producto en el a�o 2000
Select pr.pr_name as 'Producto', SUM(pr.pr_price) as 'Total vendido en el 2000' from [dbo].[tbl_purchase] as p join
[dbo].[tbl_purchase_stock_details] as d on p.pur_id = d.psd_purchase_id join
[dbo].[tbl_stock] as st on d.psd_stock_id = st.st_id join
[dbo].[tbl_product] as pr on pr.pr_id = st.st_product_id
where YEAR(p.pur_date)=2000 group by pr.pr_name


-- Obtener la �ltima fecha de compra de un cliente y seg�n su frecuencia de compra
--estimar en qu� fecha podr�a volver a comprar.
 Select FORMAT(MAX(p.pur_date),'MMM dd yyyy') AS 'Ultima fecha de compra',
format( DATEADD(DAY,DATEDIFF(DAY,(Select tp.pur_date from [dbo].[tbl_purchase] as tp where tp.pur_id = ident_current('tbl_purchase')-1),
 max(p.pur_date))/(2) 
 ,max(p.pur_date)),'MMM dd yyyy') as 'Posible proxima compra'
 from [dbo].[tbl_purchase] as p
 where p.pur_cli_id = '1073825150';





