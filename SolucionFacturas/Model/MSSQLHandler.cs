﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class MSSQLHandler
    {

        //Organizamos consultas por región

        #region Factura CRUD
        public Factura consultarFactura(int idFactura)
        {
            Factura result = new Factura();
            using (AppFacturacionEntities dc = new AppFacturacionEntities())
            {

                tbl_purchase SelectQuery =
                    (from p in dc.tbl_purchase
                     where p.pur_id == idFactura
                     select p).FirstOrDefault();

                if (SelectQuery != null)
                {
                    //Consultamos la factura
                    List<tbl_purchase_stock_details> SelectQuery2 =
                    (from p in dc.tbl_purchase_stock_details
                     where p.psd_purchase_id == idFactura
                     select p).ToList();
                    //Consultamos articulos de la factura y creamos objeto
                    if (SelectQuery2 != null)
                        foreach (tbl_purchase_stock_details d in SelectQuery2)
                            result.articulos.Add(d.psd_stock_id);
                    result.cliente = SelectQuery.pur_cli_id;
                    result.fecha = SelectQuery.pur_date.ToShortDateString();
                    result.id = SelectQuery.pur_id;
                    result.totalAPagar = SelectQuery.pur_total_price;
                    result.totalPagado = SelectQuery.pur_paid_price;

                }

                return result;
            }

        }

        public int CrearFactura(string clientID, float TotalAPagar, float TotalPagado, DateTime fecha, List<int> articulos)
        {

            using (AppFacturacionEntities dc = new AppFacturacionEntities())
            {
                //Creamos la factura
                dc.tbl_purchase.Add(new tbl_purchase()
                {
                    pur_cli_id = clientID,
                    pur_date = fecha,
                    pur_paid_price = TotalPagado,
                    pur_total_price = TotalAPagar,

                });
                //Guardamos factura
                try
                {
                    dc.SaveChanges();
                    
                }
                catch (Exception)
                {
                    return -1;
                }
                //Traemos el ultimo id
                int idPur = (from p in dc.tbl_purchase
                             select p.pur_id).Max();

                //Escribimos sus articulos 
                foreach (int i in articulos)
                    dc.tbl_purchase_stock_details.Add(new tbl_purchase_stock_details()
                    {
                        psd_purchase_id = idPur,
                        psd_stock_id = i

                    });

                //Guardamos articulos
                try
                {
                    dc.SaveChanges();
                    return 0;
                    
                }
                catch (Exception)
                {
                    return -1;
                }


            }

        }

        public int EliminarFactura(int id)
        {

            using (AppFacturacionEntities dc = new AppFacturacionEntities())
            {
                //Consultamos la factura
                tbl_purchase toDelete = (from p in dc.tbl_purchase
                                         where p.pur_id == id
                                         select p).FirstOrDefault();

                if (toDelete != null)
                {
                    //Consultamos los articulos de la factura
                    List<tbl_purchase_stock_details> articulos = (from p in dc.tbl_purchase_stock_details
                                                                  where p.psd_purchase_id == id
                                                                  select p).ToList();

                    //Si la factura tiene articulos comprados, primero borramos estos.
                    if (articulos != null)
                        foreach (tbl_purchase_stock_details i in articulos)
                            try
                            {
                                dc.tbl_purchase_stock_details.Remove(i);
                                dc.SaveChanges();
                            }
                            catch (Exception)
                            {
                                return -1;
                            }
                    //Luego si, borramos la factura
                    try
                    {
                        dc.tbl_purchase.Remove(toDelete);
                        dc.SaveChanges();
                        return 0;
                    }
                    catch (Exception)
                    {
                        return -1;
                    }
                }
                else
                { return -1; }
            }


        }

        #endregion

        #region Productos CRUD
        public Producto consultarProducto(int idProducto)
        {
            Producto result = new Producto();
            using (AppFacturacionEntities dc = new AppFacturacionEntities())
            {

                tbl_product SelectQuery =
                    (from p in dc.tbl_product
                     where p.pr_id == idProducto
                     select p).FirstOrDefault();

                if (SelectQuery != null)
                {
                  
                    result.id = idProducto;
                    result.nombre = SelectQuery.pr_name;
                    result.precio = SelectQuery.pr_price.ToString();

                }

                return result;
            }

        }

        public int CrearProducto(string nombre, float precio)
        {

            using (AppFacturacionEntities dc = new AppFacturacionEntities())
            {
                //Creamos el objeto
                dc.tbl_product.Add(new tbl_product()
                {
                    pr_name = nombre,
                    pr_price = precio

                });
                //Guardamos objeto
                try
                {
                    dc.SaveChanges();
                    return 0;
                }
                catch (Exception)
                {
                    return -1;
                }
  
            }

        }

        public int EditarProducto(int id, string nombre, float precio)
        {

            using (AppFacturacionEntities dc = new AppFacturacionEntities())
            {
                //Buscamos el producto a editar
                tbl_product SelectQuery =
                   (from p in dc.tbl_product
                    where p.pr_id == id
                    select p).FirstOrDefault();

                if (SelectQuery != null)
                {
                    SelectQuery.pr_price = precio;
                    SelectQuery.pr_name = nombre;
                    
                    //Guardamos objeto
                    try
                    {
                        dc.SaveChanges();
                        return 0;
                    }
                    catch (Exception)
                    {
                        return -1;
                    }
                } else { return -1; }
            }

        }

        public int EliminarProducto(int id)
        {

            using (AppFacturacionEntities dc = new AppFacturacionEntities())
            {
                //Consultamos la factura
                tbl_product toDelete = (from p in dc.tbl_product
                                         where p.pr_id == id
                                         select p).FirstOrDefault();

                if (toDelete != null)
                {
                    //Consultamos los items disponibles en stock
                    List<tbl_stock> articulos = (from p in dc.tbl_stock
                                                                  where p.st_product_id == id
                                                                  select p).ToList();

                    //Si la factura tiene articulos comprados, primero borramos estos.
                    if (articulos != null)
                        foreach (tbl_stock i in articulos)
                        {
                            List<tbl_purchase_stock_details> articulosComprados = (from p in dc.tbl_purchase_stock_details
                                                                          where p.psd_stock_id == i.st_id
                                                                          select p).ToList();

                            //Si la factura tiene articulos comprados, primero borramos estos.
                            if (articulosComprados != null)
                                foreach (tbl_purchase_stock_details s in articulosComprados)
                                    try
                                    {
                                        dc.tbl_purchase_stock_details.Remove(s);
                                        dc.SaveChanges();
                                    }
                                    catch (Exception)
                                    {
                                        return -1;
                                    }
                            try
                            {
                                dc.tbl_stock.Remove(i);
                                dc.SaveChanges();
                            }
                            catch (Exception)
                            {
                                return -1;
                            }

                        }
                  
                   
                    //Luego si, borramos la el producto
                    try
                    {
                        dc.tbl_product.Remove(toDelete);
                        dc.SaveChanges();
                        return 0;
                    }
                    catch (Exception)
                    {
                        return -1;
                    }
                }
                else
                { return -1; }
            }


        }

        #endregion

    }

}
