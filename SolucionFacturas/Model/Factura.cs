﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
   public class Factura
    {
        public Factura()
        {
            this.articulos = new List<int>();
        }
        public int id { get; set; }
        public string cliente { get; set; }
        public string fecha { get; set; }
        public double totalAPagar { get; set; }
        public double totalPagado { get; set; }
        public List<int> articulos { get; set; }
    }
}
