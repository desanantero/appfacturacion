﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using System.Web.Http.Description;
using Model;
using Newtonsoft.Json;

namespace WSFactura.Controllers
{
    public class ProductoController : ApiController
    {
        MSSQLHandler SQL = new MSSQLHandler();
        // GET api/producto/5
        public IHttpActionResult GetProducto(int id)
        {

            Producto f = new Producto();
            f = SQL.consultarProducto(id);
            if (f.nombre == null)
                return NotFound();
            string rawJsonFromDb = JsonConvert.SerializeObject(f);
            var response = Request.CreateResponse(HttpStatusCode.OK);
            response.Content = new StringContent(rawJsonFromDb, Encoding.UTF8, "application/json");
            return Ok(f);
        }

        // POST api/producto
        [ResponseType(typeof(Producto))]
        public IHttpActionResult PostProducto(Producto nuevoProducto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            int r = SQL.CrearProducto(nuevoProducto.nombre, float.Parse(nuevoProducto.precio));
            if (r >= 0)
                return Ok();
            else
                return InternalServerError();

        }


        // DELETE api/producto/5
        public IHttpActionResult DeleteProducto(int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            int r = SQL.EliminarProducto(id);
            if (r >= 0)
                return Ok();
            else
                return InternalServerError();
        }

        [ResponseType(typeof(Producto))]
        public IHttpActionResult PutProducto(Producto ProductoEditado)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            int r = SQL.EditarProducto(ProductoEditado.id,ProductoEditado.nombre, float.Parse(ProductoEditado.precio));
            if (r >= 0)
                return Ok();
            else
                return InternalServerError();

        }
    }
}