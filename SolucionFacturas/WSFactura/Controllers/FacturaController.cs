﻿using Microsoft.AspNetCore.Mvc;
using Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using System.Web.Http.Description;

namespace WSFactura.Controllers
{
    public class FacturaController : ApiController
    {

        MSSQLHandler SQL = new MSSQLHandler();
        // GET api/factura/5
        public IHttpActionResult GetFactura(int id)
        {

            Factura f = new Factura();
            f = SQL.consultarFactura(id);
            if (f.cliente == null)
                return NotFound();
            string rawJsonFromDb = JsonConvert.SerializeObject(f);
            var response = Request.CreateResponse(HttpStatusCode.OK);
            response.Content = new StringContent(rawJsonFromDb, Encoding.UTF8, "application/json");
            return Ok(f);
        }

        // POST api/factura
        [ResponseType(typeof(Factura))]
        public IHttpActionResult PostFactura(Factura nuevaFactura)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            int r = SQL.CrearFactura(nuevaFactura.cliente, float.Parse(nuevaFactura.totalAPagar.ToString()), float.Parse(nuevaFactura.totalPagado.ToString()), Convert.ToDateTime(nuevaFactura.fecha), nuevaFactura.articulos);
            if (r >= 0)
                return Ok();
            else
                return InternalServerError();

        }

      
        // DELETE api/factura/5
        public IHttpActionResult DeleteFactura(int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            int r = SQL.EliminarFactura(id);
            if (r >= 0)
                return Ok();
            else
                return InternalServerError();
        }
    }
}
